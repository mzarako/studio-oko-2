import React, { Component } from 'react';
import { CSSTransitionGroup } from 'react-transition-group';

export default function Image(props) {
  return (
      <CSSTransitionGroup
        transitionName="fade-in"
        transitionAppear transitionLeave
        transitionAppearTimeout={1100}
        transitionEnterTimeout={0}
        transitionLeaveTimeout={0}>
          {props.children}
      </CSSTransitionGroup>
  )
}
