import React, { Component } from 'react';
import { ParallaxContainer, Parallax } from "react-gsap-parallax";
import { Linear, Power4, Sine, Circ } from 'gsap';
import scrollTo from 'scroll-to-element';
import logo from '../../../assets/logo.svg';
import whiteArrow from '../../../assets/white-arrow.svg';
import Menu from './menu.desktop';
import Contact from './contact.desktop';
import CaseStudies from './case-studies.desktop';
import About from './about.desktop';
import Landing from './landing.desktop';
import Experiments from './experiments.desktop';


class Home extends Component {
  constructor() {
    super();
    this.state = {
      scrollable: true,
      maskAbout: false,
      maskCaseStudies: false,
      maskExperiments: false
    };
    // this.handleOnWheel = this.handleOnWheel.bind(this);
    this.handleMasks = this.handleMasks.bind(this);
    this.maskAbout = this.maskAbout.bind(this);
    this.maskCaseStudies = this.maskCaseStudies.bind(this);
    this.maskExperiments = this.maskExperiments.bind(this);
  }
  // handleOnWheel(e) {
  //   if (Math.abs(e.deltaY) > 5 && this.state.scrollable) {
  //     this.setState({ scrollable: false });
  //     console.log(e.deltaY);
  //     this.scrollTo();
  //   }
  // }
  // scrollTo() {
  //   setTimeout( ()=> { this.setState({ scrollable: true }) }, 1000);
  // }
  handleMasks(mask) {
    if (mask === 'about' || mask === 'case studies' || mask === 'experiments') {
      this.setState({ maskAbout: true });
      setTimeout(()=> this.setState({ maskAbout: false }), 1000);
    }
    if (mask === 'case studies' || mask === 'experiments') {
      this.setState({ maskCaseStudies: true });
      setTimeout(()=> this.setState({ maskCaseStudies: false }), 1000);
    }
    if (mask === 'experiments') {
      this.setState({ maskExperiments: true });
      setTimeout(()=> this.setState({ maskExperiments: false }), 1000);
    }
  }
  maskAbout() {
    if (this.state.maskAbout) {
      return (
        <Parallax style={{ ...styles.fullPage, ...styles.mask }} keyframes={keyframes.about}>
          <div style={ styles.mask }></div>
        </Parallax>
      )
    }
    else return (
      <Parallax style={{ hieght: '0' }} keyframes={keyframes.about}>
        <div></div>
      </Parallax>
    );
  }
  maskCaseStudies() {
    if (this.state.maskCaseStudies) {
      return (
        <Parallax style={{ ...styles.fullPage, ...styles.mask }} keyframes={keyframes.caseStudies}>
          <div style={ styles.mask }></div>
        </Parallax>
      )
    }
    else return (
      <Parallax style={{ hieght: '0' }} keyframes={keyframes.caseStudies}>
        <div></div>
      </Parallax>
    );
  }
  maskExperiments() {
    if (this.state.maskExperiments) {
      return (
        <Parallax style={{ ...styles.fullPage, ...styles.mask }} keyframes={keyframes.experiments}>
          <div style={ styles.mask }></div>
        </Parallax>
      )
    }
    else return (
      <Parallax style={{ hieght: '0' }} keyframes={keyframes.experiments}>
        <div></div>
      </Parallax>
    );
  }
  componentWillMount() {
    this.props.showLanding(true);
  }
  render() {
    return (
      <div className="home">
        <ParallaxContainer height={scrollHeight} top={0} scrolljack={false} onScroll={x => x} style={{ position: 'static' }}>

          <Parallax style={ styles.menuAnchor } keyframes={keyframes.anchor}>
            <div className="menu-anchor"></div>
          </Parallax>
          <Parallax style={ styles.aboutAnchor } keyframes={keyframes.anchor}>
            <div className="about-anchor"></div>
          </Parallax>
          <Parallax style={ styles.caseStudiesAnchor } keyframes={keyframes.anchor}>
            <div className="case-studies-anchor"></div>
          </Parallax>
          <Parallax style={ styles.contactAnchor } keyframes={keyframes.anchor}>
            <div className="contact-anchor"></div>
          </Parallax>


          <Parallax style={ styles.landing } keyframes={keyframes.landing}>
            <Landing/>
          </Parallax>


          <Parallax style={ styles.fullPage } keyframes={keyframes.menu}>
            <Menu handleMasks={this.handleMasks}/>
          </Parallax>

          <Parallax style={ styles.fullPage } keyframes={keyframes.about}>
            <About/>
          </Parallax>
          {this.maskAbout()}

          <Parallax style={ styles.fullPage } keyframes={keyframes.caseStudies}>
            <CaseStudies/>
          </Parallax>
          {this.maskCaseStudies()}

          <Parallax style={ styles.fullPage } keyframes={keyframes.experiments}>
            <Experiments/>
          </Parallax>
          {this.maskExperiments()}

          <Parallax style={ styles.fullPage } keyframes={keyframes.contact}>
            <Contact/>
          </Parallax>
        </ParallaxContainer>
      </div>
    )
  }
}

const styles =  {
  landing: {
    width: '100%',
    maxWidth: '1400px',
    top: "0"
  },
  fullPage: {
    width: '100%',
    maxWidth: '1400px',
    top: "100vh"
  },
  menuAnchor: {
    top: '1024px',
    width: '1px',
    height: '1px'
  },
  aboutAnchor: {
    width: '1px',
    height: '1px',
    top: '1024px'
  },
  caseStudiesAnchor: {
    top: '2048px',
    width: '1px',
    height: '1px'
  },
  experimentsAnchor: {
    top: '3072px',
    width: '1px',
    height: '1px'
  },
  contactAnchor : {
    top: '6390px',
    width: '1px',
    height: '1px'
  },
  mask: {
    height: '100vh',
    backgroundColor: '#212121'
  }
}

const scrollHeight = 6400;

function pin(position) {
  const deltaY = scrollHeight / 4;
  const transY = deltaY / 1.5;
  const start = deltaY * (position - 1) - transY;
  const pinStart = start + transY;
  const pinEnd = deltaY * position - transY;
  const end = pinEnd + transY;
  return [ (start / 100).toFixed(3) + '%', (pinStart / 100).toFixed(3) + '%', (pinEnd / 100).toFixed(3) + '%', (end / 100).toFixed(3) + '%' ];
}

function pinPage(position) {
  const pinIt = pin(position);
  const pinObj = {};
  pinObj[`${pinIt[0]}`] = { transform: "translateY(0)", ease: Sine.easeInOut };
  pinObj[`${pinIt[1]}`] = { transform: "translateY(-100vh)", ease: Sine.easeInOut };
  pinObj[`${pinIt[2]}`] = { transform: "translateY(-100vh)", ease: Sine.easeInOut };
  pinObj[`${pinIt[3]}`] = { transform: "translateY(-200vh)", ease: Sine.easeInOut };
  return pinObj;
}

function pinStart() {
  const pinIt = pin(1);
  const pinObj = {};
  pinObj[`${pinIt[2]}`] = { transform: "translateY(0)", ease: Sine.easeInOut };
  pinObj[`${pinIt[3]}`] = { transform: "translateY(-100vh)", ease: Sine.easeInOut };
  return pinObj;
}

function pinEnd() {
  const pinIt = pin(6);
  const pinObj = {};
  pinObj[`${pinIt[0]}`] = { transform: "translateY(0)", ease: Sine.easeInOut };
  pinObj[`${pinIt[1]}`] = { transform: "translateY(-100vh)", ease: Sine.easeInOut };
  return pinObj;
}

const keyframes = {
  landing: pinStart(),
  menu: pinPage(2),
  about: pinPage(3),
  caseStudies: pinPage(4),
  experiments: pinPage(5),
  contact: pinEnd(),
  anchor: { "100%": { opacity: "1" }}
}


console.log('keyframes.landing', keyframes.landing);
console.log('keyframes.menu', keyframes.menu);
console.log('keyframes.about', keyframes.about);
console.log('keyframes.caseStudies', keyframes.caseStudies);
console.log('keyframes.contact', keyframes.contact);



export default Home;
