import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FadeIn from '../fade-in';
import london from '../../../assets/kitaj/LondonByNight.jpg';
import inTheMountains from '../../../assets/kitaj/in-the-mountains.jpg';
import self from '../../../assets/kitaj/self.jpg';
import jewishRider from '../../../assets/kitaj/jewish-rider.jpg';
import arcade from '../../../assets/kitaj/arcade.jpg';
import kitajMacs from '../../../assets/kitaj/kitaj-macs-01.jpg';
import landing1 from '../../../assets/kitaj/landing1.jpg';
import landing2 from '../../../assets/kitaj/landing2.jpg';
import slideshow from '../../../assets/kitaj/slideshow.jpg';
import masonry from '../../../assets/kitaj/masonry.jpg';
import exhibitions from '../../../assets/kitaj/exhibitions.jpg';
import biography from '../../../assets/kitaj/biography.jpg';

class KitajDesktop extends Component {
  constructor() {
    super();
  }
  componentWillMount() {
    this.props.showLanding(false);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
        <div className="case-study black-2">
          <Link to="/" className="back-X"><h3>X</h3></Link>

          <div className="white-text padding centered">

            <div className="header">
              <FadeIn>
                <img className="img-top" src={london} />
              </FadeIn>

              <div className="name-box box-kitaj">
                <h4>r.b. kitaj</h4>
              </div>
            </div>


            <h5>R.B. Kitaj (1932-2007) was an American painter, draftsman, and writer.</h5>
            <br/>
            <h5>We designed and built a website about Kitaj, commissioned by his estate, the R.B. Kitaj Studio Project.</h5>

            <FadeIn>
              <div className="two-imgs kitaj-two-imgs">
                <div className="img-container flex-end"><img src={jewishRider} /></div>
                <div className="spacer-div"></div>
                <div className="img-container"><img src={self} /></div>
              </div>
            </FadeIn>

            <h5>When we were approached about the project, we began our research into Kitaj– the man and the artist. We were immediately immersed in rich, bright colors, themes of politics and identity, and a life devoted to art. The artist’s estate wanted a website that encapsulated all of this to preserve Kitaj’s legacy. </h5>
          </div>

          <div className="white-triangle-up"></div>
          <div className="white padding centered">

            <div style={{display: "inline-block", margin: "auto"}}><img src={kitajMacs} className="max-width-1020" /></div>

            <br/>
            <p className="screen-up-padding-desktop"><br/><br/><span>We were handed a stack of books and catalogues</span> when we met with Tracy, from the R.B. Kitaj Studio Project. As someone who knew Kitaj closely, she was able to clue us in on his routines, his values, and personal details– like the fact that he preferred when art would bleed off a page. We talked about creating a site that felt dynamic, full of life, unafraid of color. The site also had to feel welcoming and suitable for its varied vistors– long-time fans, museum curators, students, and galleries. The estate wanted the site to act as a one-stop headquarters of all things Kitaj and needed some help bringing structure to the massive body of work. </p>

          </div>
          <div className="white-triangle-down"></div>

          <div className="padding white-text z2 centered">
            <div className="screen-up-desktop"><img src={landing2}/></div>
            <h5>Our goal was to make a website that reflected the identity and the art of R.B. Kitaj– a site that he would have commissioned himself. </h5>
            <div className="screen-down-desktop"><img src={slideshow}/></div>
          </div>

          <div className="white-triangle-left"></div>
          <div className="white padding centered">
            <p className="screen-down-padding-desktop screen-up-padding-desktop"><span>We started the design process with some interesting problems</span> to solve. From an archive of different art mediums to an extensive list of publications, there was a lot to organize and make digestible. By thinking of the user first, we broke things down into manageable categories and organized Kitaj’s artworks by medium and period. We gave each page a color identity using swatches pulled from Kitaj’s own works. We wanted the artwork to be displayed in a non-traditional way, and loved how a masonry view could quickly give visitors a good feel for Kitaj. We included a slideshow view for visitors who wanted to look at each piece independently. To tell the story of his life, we broke down his decades of painting into early, middle, and late and matched each with snapshots and paintings from that time. The final touch was using Kitaj’s own signature as the site logo.</p>
          </div>
          <div className="white-triangle-right"></div>

          <div className="white-text padding z2 centered">
            <div className="screen-up-desktop"><img src={masonry}/></div>
            <h5>To organize an enormous body of distinct works, we took an approach that was both logical and empathic. If done right, the site would be equally accesible to a Kitaj newbie and an art intellectual. </h5>
            <div className="screen-down-desktop"><img src={exhibitions}/></div>
          </div>

          <div className="white-triangle-up"></div>

          <div className="white padding centered">
            <p className="screen-up-padding-desktop screen-down-padding-desktop"><span>The technical priorities for this project </span> focused on creating a user-friendly Content Management System. The estate wanted to be able to continually add images– designating them to a certain medium and period, exhibition information and news items. We built a CMS using Contentful that is actually fun to use and requires no technical saviness.</p>
          </div>

          <div className="white-triangle-down"></div>

          <div className="white-text padding z2">
            <div className="screen-up-desktop"><img src={biography}/></div>
            <Link to="/"><h5 style={{textAlign: "center", padding: "60px 0"}}>take me home</h5></Link>
          </div>
        </div>
    )
  }
}

export default KitajDesktop;
