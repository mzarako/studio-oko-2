import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import MediaQuery from 'react-responsive';

class About1Mobile extends Component {
  constructor() {
    super();
    this.getUprightAngle = this.getUprightAngle.bind(this);
    this.getUpsideDownAngle = this.getUpsideDownAngle.bind(this);
  }
  getUprightAngle(h) {
    const width = window.innerWidth;
    console.log(width);
    const hypot = Math.sqrt( width*width + h*h );
    const rad = Math.acos( width / hypot );
    const angle = 180 + rad*180/Math.PI;
    return { transform: `rotate(${angle}deg)` };
  }
  getUpsideDownAngle(h) {
    const width = window.innerWidth;
    console.log(width);
    const hypot = Math.sqrt( width*width + h*h );
    const rad = Math.acos( width / hypot );
    const angle = rad*180/Math.PI;
    return { transform: `rotate(${angle}deg)` };
  }
  render() {
    return (
        <div className="about about1 black">
          <div className="header">
              <div className="triangle"></div>
              <MediaQuery maxWidth={699}>
                <h2 className="upright" style={this.getUprightAngle(160)}>ABOUT</h2>
              </MediaQuery>
              <MediaQuery maxWidth={699}>
                <h2 className="upside-down" style={this.getUpsideDownAngle(160)}>ABOUT</h2>
              </MediaQuery>
              <MediaQuery minWidth={700}>
                <h2 className="upright" style={this.getUprightAngle(200)}>ABOUT</h2>
              </MediaQuery>
              <MediaQuery minWidth={700}>
                <h2 className="upside-down" style={this.getUpsideDownAngle(200)}>ABOUT</h2>
              </MediaQuery>


          </div>
            <p><span>Studio OKO is a pocket-sized web design and development studio</span> in Los Angeles specializing in crafting custom sites for artists, businesses, and non-profits seeking a distinctive web presence.<br /><br />

              No matter the project, our approach is simple– combine imagination, problem solving, and a meticulous eye for a virtual experience that won’t disappoint.</p>
        </div>
    )
  }
}

export default About1Mobile;
