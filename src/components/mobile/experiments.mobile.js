import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import orange1 from '../../../assets/automaton/orange1.jpg';


class ExperimentsMobile extends Component {
  constructor() {
    super();
  }
  render() {
    return (
        <div className="experiments black">

          <div className="padding">

            <div className="header">
              <h2>EXPERIMENTS</h2>
            </div>

            <Link to="/automaton">
            <div className="relative">
              <div className="name-box">
                <h4>cellular automaton</h4>
              </div>
              <img src={orange1} />
            </div>
            </Link>

        </div>
      </div>
    )
  }
}

export default ExperimentsMobile;
