import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class ContactMobile extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      message: '',
      submitMessage: ''
    }
    this.onNameChange = this.onNameChange.bind(this);
    this.onEmailChange = this.onEmailChange.bind(this);
    this.onMessageChange = this.onMessageChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.validateForm = this.validateForm.bind(this);
  }
  onNameChange(event) {
    this.setState({ name: event.target.value });
  }
  onEmailChange(event) {
    this.setState({ email: event.target.value });
  }
  onMessageChange(event) {
    this.setState({ message: event.target.value });
  }
  onSubmit(event) {
    event.preventDefault();
    const { name, email, message } = this.state;

    const validated = this.validateForm(name, email, message);

    if (validated) {
      axios.post('//formspree.io/info@studiooko.co', { name, email, message })
        .then( response => {
          console.log(response);
          this.setState({ name: '', email: '', website: '', message: '', submitMessage: 'Thanks! Talk soon.' });
        })
        .catch( error => {
          console.log(error);
          this.setState({ submitMessage: 'Oops! Try again.' });
        });
    }
  }
  validateForm(name, email, message) {
    if (name === '') {
      this.setState({ submitMessage: "What's your name?"});
      return false;
    }
    const validEmail = /\S+@\S+\.\S+/;
    if (!validEmail.test(email)) {
      this.setState({ submitMessage: 'Check your email.'});
      return false;
    }
    if (message === '') {
      this.setState({ submitMessage: 'Write a message.'});
      return false;
    }
    return true;
  }
  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <div className="contact white">
          <div className="title"><h2>CONTACT</h2></div>



            <div className="name-email-container white-text">
              <div className="name black white-text">

                  <h4>name:</h4>
                  <input name="name" type="text" value={this.state.name} onChange={this.onNameChange} />

              </div>

              <div className="email black white-text">

                  <h4>email:</h4>
                  <input name="email" type="email" value={this.state.email} onChange={this.onEmailChange} />

              </div>
            </div>

            <div className="message black white-text">

              <div className="submit-message"><h4>{this.state.submitMessage}</h4></div>

              <label><h4>message:</h4>
                <textarea name="message" value={this.state.message} onChange={this.onMessageChange} />
              </label>

              <div className="underlines-container">
                <div className="underline"></div>
                <div className="underline"></div>
                <div className="underline"></div>
                <div className="underline"></div>
              </div>

              <div className="send-triangle"></div>
              <h3 className="white-font" onClick={this.onSubmit}>SEND IT</h3>

            </div>



        </div>
        </form>
    )
  }
}

export default ContactMobile;
