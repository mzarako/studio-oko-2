import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import orange1 from '../../../assets/automaton/orange1.jpg';
import dark1 from '../../../assets/automaton/dark1.jpg';
import dark2 from '../../../assets/automaton/dark2.jpg';
import dark3 from '../../../assets/automaton/dark3.jpg';
import lava1 from '../../../assets/automaton/lava1.jpg';
import lava2 from '../../../assets/automaton/lava2.jpg';
import lava3 from '../../../assets/automaton/lava3.jpg';
import lavender1 from '../../../assets/automaton/lavender1.jpg';
import lavender2 from '../../../assets/automaton/lavender2.jpg';
import lavender3 from '../../../assets/automaton/lavender3.jpg';
import mint1 from '../../../assets/automaton/mint1.jpg';
import mint2 from '../../../assets/automaton/mint2.jpg';
import mint3 from '../../../assets/automaton/mint3.jpg';
import orange2 from '../../../assets/automaton/orange2.jpg';
import orange3 from '../../../assets/automaton/orange3.jpg';
import orange4 from '../../../assets/automaton/orange4.jpg';
import southwest1 from '../../../assets/automaton/southwest1.jpg';
import southwest2 from '../../../assets/automaton/southwest2.jpg';
import southwest3 from '../../../assets/automaton/southwest3.jpg';
import webpage from '../../../assets/automaton/webpage.png';


class AutomatonMobile extends Component {
  constructor() {
    super();
  }
  render() {
    return (
        <div className="automaton">
          <Link to="/" className="back-X"><h3>X</h3></Link>
          <div className="padding">

            <div className="relative">
              <img src={orange1} />

              <div className="name-box">
                <h4>cellular automaton</h4>
              </div>
            </div>

            <h5>One of our favorite passion projects is a custom cellular automaton generator.</h5>
            <br/><br/>
            <h5>What's a cellular automaton you ask? The short answer: a system of cells that follow some rules and makes interesting patterns.</h5>

            <div className="webpage"><img src={webpage} /></div>

            <h5>Above is a glance at the site we made. On it you can change the color scheme, choose the initial state of the cells, switch through seven rulesets that change the pattern, adjust the speed, and finally, add a little randomness. Whew!</h5>

            </div>
            <div className="white">
              <div className="padding">

                <h5>Since we've spent countless hours playing with our creation, we've stumbled upon some really groovy compositions. Here are some of our favorites.</h5>

                <div className="favorites">
                  {/* <div className="favorites-img"><img src={southwest1} /></div> */}
                  <div className="favorites-img"><img src={lavender1} /></div>
                  <div className="favorites-img"><img src={mint1} /></div>
                  <div className="favorites-img"><img src={southwest2} /></div>
                  <div className="favorites-img"><img src={lavender2} /></div>
                  <div className="favorites-img"><img src={mint2} /></div>
                  <div className="favorites-img"><img src={southwest3} /></div>
                  <div className="favorites-img"><img src={lavender3} /></div>
                  <div className="favorites-img"><img src={mint3} /></div>
                </div>
              </div>
            </div>

            <div className="padding">
              <div className="favorites">
                <div className="favorites-img"><img src={dark1} /></div>
                <div className="favorites-img"><img src={lava1} /></div>
                <div className="favorites-img"><img src={orange3} /></div>
                <div className="favorites-img"><img src={dark2} /></div>
                <div className="favorites-img"><img src={lava2} /></div>
                <div className="favorites-img"><img src={orange4} /></div>
                <div className="favorites-img"><img src={dark3} /></div>
                <div className="favorites-img"><img src={lava3} /></div>
                {/* <div className="favorites-img"><img src={orange4} /></div> */}
              </div>
            </div>


          <Link to="http://cellularautomaton.science/" target="_blank"><h5 style={{textAlign: "center", padding: "60px 0 5px 0"}}>take me to the automaton</h5></Link>
          <Link to="/"><h5 style={{textAlign: "center", padding: "5px 0 60px 0"}}>take me home</h5></Link>
        </div>
    )
  }
}

export default AutomatonMobile;
